/*
package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Test;

public class BusinessShipmentTest {

    @Test
    public void shouldCreateANewBusinessAccount(){
        //Arrange
        Business sut = new Business("name","phone","address","e-mail");

        //Act

        //Assert
        Assert.assertEquals("name",sut.getName());

    }

    @Test
    public void shouldCreateNewShipmentByBusinessAccount(){
        //Arrange
        BusinessShipment sut = new BusinessShipment();
        Business business = new Business("name","phone","address","e-mail");
        sut.createAccount(business);
        Person receiver = new Person("sibel", "can","05053334466", new Address("il","ilce","sokak","numara"));
        ShipmentPackage shipmentPackage = new ShipmentPackage(10,2,4,10);

        //Act
        sut.createShipment(business.getId());
        sut.setReceiver(receiver);
        sut.setShipmentPackage(shipmentPackage);
        sut.setShipmentType("box");
        sut.setPayer("Sender");

        //Assert
        Assert.assertEquals(sut.getBusinessId(),business.getId());
    }
}
*/

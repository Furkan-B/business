package com.example.demo.service;

import com.example.demo.domain.Business;
import com.example.demo.exception.BusinessNotCreatedException;
import com.example.demo.exception.BusinessNotFoundException;
import com.example.demo.repository.BusinessRepository;
import org.springframework.stereotype.Service;

@Service
public class BusinessService {
    private final BusinessRepository businessRepository;

    public BusinessService(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }

    public void createBusinessAccount(Business business) throws BusinessNotCreatedException {
        businessRepository.insertBusinessAccount(business);
    }


    public Business getBusinessAccountByID(String id) throws BusinessNotFoundException {
        return businessRepository.getBusinessAccountByID(id);
    }

    public void deleteBusinessAccount(String id) throws BusinessNotFoundException {
        businessRepository.deleteBusinessAccountByID(id);
    }

}

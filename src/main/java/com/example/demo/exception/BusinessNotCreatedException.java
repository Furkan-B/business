package com.example.demo.exception;

public class BusinessNotCreatedException extends Exception{
    public BusinessNotCreatedException(String message){
        super(message);
    }
}

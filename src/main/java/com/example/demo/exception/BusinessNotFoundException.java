package com.example.demo.exception;

public class BusinessNotFoundException extends Exception{
    public BusinessNotFoundException(String message){
        super(message);
    }
}
